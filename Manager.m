classdef Manager
    methods (Static)
        function OfferALoan()
            disp('Looks like you have insufficient balance. Would you like a loan?');
        end
        function ObserveCustomer(account)
            addlistener(account,'NegativeBalance',@(src,e) Manager.OfferALoan())
        end
    end
end
