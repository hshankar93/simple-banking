			Simple Banking Transactions

These programs allow the instantiation of a rudimentary banking system 
consisting of 2 classes, Account and Manager.

Setup
`````
To create a new Bank account, the user name and initial balance should be 
provided at the time of instantiation.
ex. JohnDoeAccount = Account('John Doe',1000)

To create a new Manager, we just instantiate the Manager class with no 
parameters and make him observe a bank account.
ex. Manager1 = Manager
    Manager1.ObserveCustomer(JohnDoeAccount)
   
However, since the Manager methods are static there is no need to create
an instance of Manager to perform the functions.


Functionalities
```````````````
Account - 
1] Deposit - Deposit money into account.
2] Withdraw - Withdray money from account.
3] BuyLottery - Buy lottery to win some random amount of money.
4] Greet - Say hello. (Static)

Account also has an event called 'NegativeBalance' that alerts the Manager observing
the account when the balance is too low to make a withdrawal.

Manager - 
1] OfferALoan - Offer a loan automatically to the customer when balance is insufficient.
2] ObserveCustomer - Hitches the Manager and the user, to monitor account balance.



