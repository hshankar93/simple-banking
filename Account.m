classdef Account < handle
    properties
        Name;
        Balance;
    end
    events
        NegativeBalance
    end
    methods
        % Initialize using Constructor
        function obj = Account(NameInput,BalanceInput) 
            obj.Name = NameInput;
            obj.Balance = BalanceInput;
        end
        function Deposit(obj,amount)
            obj.Balance = obj.Balance + amount;
        end        
        function Withdraw(obj,amount)
            if amount <= obj.Balance
                obj.Balance = obj.Balance - amount;
            else
                % Alert the observing manager that user has no balance.
                notify(obj,'NegativeBalance'); 
            end
        end
        % Calls RandomizeLotter, an Anonymous function that generates random lottery win 
        function BuyLottery(obj)         
            oldBalance = obj.Balance;
            RandomizeLottery = @(amount) (0.5\rand)*(10)*(amount);
            obj.Balance = round(obj.Balance+RandomizeLottery(obj.Balance));
            disp(['Your balance has increased from ' num2str(oldBalance) ' to ' num2str(obj.Balance)]);
        end
    end
    methods (Static)
        function Greet()
            disp('Hello!');
        end
    end
end